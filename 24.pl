:- use_module(library(clpfd)).

fun4(Acc, Input, Offset, _, NewAcc) :-
    Input #= (Acc mod 26) + Offset,
    NewAcc #= Acc div 26.

fun4(Acc, Input, Offset, Offset2, NewAcc) :-
    Input #\= (Acc mod 26) + Offset,
    NewAcc #= 26 * (Acc div 26) + Input + Offset2.

fun5(Acc, Input, Offset, _, NewAcc) :-
    Input #= (Acc mod 26) + Offset,
    NewAcc = Acc.

fun5(Acc, Input, Offset, Offset2, NewAcc) :-
    Input #\= (Acc mod 26) + Offset,
    NewAcc #= 26 * Acc + Input + Offset2.

inputs(X) :-
    length(X, 14),
    X ins 1..9.



solve([I1, I2, I3, I4, I5, I6, I7, I8, I9, I10, I11, I12, I13, I14]) :-
    Acc1 #= I1 + 13,
    fun5(Acc1, I2, 10, 16, Acc2),
    fun5(Acc2, I3, 12, 2, Acc3),
    fun5(Acc3, I4, 10, 8, Acc4),
    fun5(Acc4, I5, 14, 11, Acc5),
    fun4(Acc5, I6, -11, 6, Acc6),
    fun5(Acc6, I7, 10, 12, Acc7),
    fun4(Acc7, I8, -16, 2, Acc8),
    fun4(Acc8, I9, -9, 2, Acc9),
    fun5(Acc9, I10, 11, 15, Acc10),
    fun4(Acc10, I11, -8, 1, Acc11),
    fun4(Acc11, I12, -8, 10, Acc12),
    fun4(Acc12, I13, -10, 14, Acc13),
    fun4(Acc13, I14, -9, 10, 0).

number([], 0).
number([H|R], Y) :-
    number(R, Y2),
    length(R, L),
    Y is Y2 + (10 ** L) * H.

solution_1(Solution) :-
    inputs(X),
    solve(X),
    labeling([ff, down, enum], X), !,
    number(X, Solution).

solution_2(Solution) :-
    inputs(X),
    solve(X),
    labeling([ff, up, enum], X), !,
    number(X, Solution).
