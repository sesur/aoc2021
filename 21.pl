:- use_module(library(clpfd)).

roll(OldPos, NewPos) :-
    Roll in 1 .. 3,
    PrePos #= X + Z,
    NewPos #= PrePos mod 10.

move(OldPos, OldScore, NewPos, NewScore) :-
    roll(OldPos, Z1),
    roll(Z1, Z2),
    roll(Z2, NewPos),
    NewScore #= OldScore + NewPos + 1.

win(Score) :- Score #>= 21.

try_win(_, _, _, Score2) :-
    win(Score2), !, fail.

try_win(Pos1, Score1, Pos2, Score2) :-
    win(Score1).

try_win(Pos1, Score1, Pos2, Score2) :-
    move(Pos1, Score1, _, NewScore),
    win(NewScore).

try_win(Pos1, Score1, Pos2, Score2) :-
    move(Pos1, Score1, NewPos1, NewScore1),
    move(Pos2, Score2, NewPos2, NewScore2),
    try_win(NewPos1, NewScore1, NewPos2, NewScore2).
