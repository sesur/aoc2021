(ns aoc.day6
  (:require [clojure.string :as string]))

(def test-input "3,4,3,1,2")

(def real-input "1,4,3,3,1,3,1,1,1,2,1,1,1,4,4,1,5,5,3,1,3,5,2,1,5,2,4,1,4,5,4,1,5,1,5,5,1,1,1,4,1,5,1,1,1,1,1,4,1,2,5,1,4,1,2,1,1,5,1,1,1,1,4,1,5,1,1,2,1,4,5,1,2,1,2,2,1,1,1,1,1,5,5,3,1,1,1,1,1,4,2,4,1,2,1,4,2,3,1,4,5,3,3,2,1,1,5,4,1,1,1,2,1,1,5,4,5,1,3,1,1,1,1,1,1,2,1,3,1,2,1,1,1,1,1,1,1,2,1,1,1,1,2,1,1,1,1,1,1,4,5,1,3,1,4,4,2,3,4,1,1,1,5,1,1,1,4,1,5,4,3,1,5,1,1,1,1,1,5,4,1,1,1,4,3,1,3,3,1,3,2,1,1,3,1,1,4,5,1,1,1,1,1,3,1,4,1,3,1,5,4,5,1,1,5,1,1,4,1,1,1,3,1,1,4,2,3,1,1,1,1,2,4,1,1,1,1,1,2,3,1,5,5,1,4,1,1,1,1,3,3,1,4,1,2,1,3,1,1,1,3,2,2,1,5,1,1,3,2,1,1,5,1,1,1,1,1,1,1,1,1,1,2,5,1,1,1,1,3,1,1,1,1,1,1,1,1,5,5,1")

(defn parse [input]
  (->> (string/split input #",")
     (mapv #(Integer/parseInt %))))

(def ^:dynamic *reset-day* -1)

(defn step [fishes]
  (reduce (fn [new [day count]]
            (if (= day 0)
              (-> new
                 (update 6 (fnil + 0) count)
                 (assoc 8 count))
              (update new (dec day) (fnil + 0) count))) {} fishes))

(defn fishes [parsed]
  (frequencies parsed))

(defn run [n input]
  (let [parsed (parse input)
        fishes (fishes parsed)]
    (loop [ctr 0
           fishes fishes]
      (if (< ctr n)
        (recur (inc ctr) (step fishes))
        (reduce + (vals fishes))))))

;;; Part 1
(defn run-test []
  (run 80 test-input))

(defn run-real []
  (run 80 real-input))

;;; Part 2
(defn run-test-2 []
  (run 256 test-input))

(defn run-real-2 []
  (run 256 real-input))
