(ns aoc.day16
  (:require [clojure.string :as string]))

(def real-input "820D4100A1000085C6E8331F8401D8E106E1680021708630C50200A3BC01495B99CF6852726A88014DC9DBB30798409BBDF5A4D97F5326F050C02F9D2A971D9B539E0C93323004B4012960E9A5B98600005DA7F11AFBB55D96AFFBE1E20041A64A24D80C01E9D298AF0E22A98027800BD4EE3782C91399FA92901936E0060016B82007B0143C2005280146005300F7840385380146006900A72802469007B0001961801E60053002B2400564FFCE25FEFE40266CA79128037500042626C578CE00085C718BD1F08023396BA46001BF3C870C58039587F3DE52929DFD9F07C9731CC601D803779CCC882767E668DB255D154F553C804A0A00DD40010B87D0D6378002191BE11C6A914F1007C8010F8B1122239803B04A0946630062234308D44016CCEEA449600AC9844A733D3C700627EA391EE76F9B4B5DA649480357D005E622493112292D6F1DF60665EDADD212CF8E1003C29193E4E21C9CF507B910991E5A171D50092621B279D96F572A94911C1D200FA68024596EFA517696EFA51729C9FB6C64019250034F3F69DD165A8E33F7F919802FE009880331F215C4A1007A20C668712B685900804ABC00D50401C89715A3B00021516E164409CE39380272B0E14CB1D9004632E75C00834DB64DB4980292D3918D0034F3D90C958EECD8400414A11900403307534B524093EBCA00BCCD1B26AA52000FB4B6C62771CDF668E200CC20949D8AE2790051133B2ED005E2CC953FE1C3004EC0139ED46DBB9AC9C2655038C01399D59A3801F79EADAD878969D8318008491375003A324C5A59C7D68402E9B65994391A6BCC73A5F2FEABD8804322D90B25F3F4088F33E96D74C0139CF6006C0159BEF8EA6FBE3A9CEC337B859802B2AC9A0084C9DCC9ECD67DD793004E669FA2DE006EC00085C558C5134001088E308A20")

(def test-inputs ["D2FE28"])

(def bits-of {\0 [0 0 0 0]
              \1 [0 0 0 1]
              \2 [0 0 1 0]
              \3 [0 0 1 1]
              \4 [0 1 0 0]
              \5 [0 1 0 1]
              \6 [0 1 1 0]
              \7 [0 1 1 1]
              \8 [1 0 0 0]
              \9 [1 0 0 1]
              \A [1 0 1 0]
              \B [1 0 1 1]
              \C [1 1 0 0]
              \D [1 1 0 1]
              \E [1 1 1 0]
              \F [1 1 1 1]})

(defn read-bits [input]
  (loop [chars input
         res   []]
    (if-let [next-char (first chars)]
      (let [bits (bits-of next-char)]
        (recur (rest chars)
               (into res bits)))
      res)))

(defn make-int [bits]
  (loop [bits bits
         num  0]
    (if-let [next (first bits)]
      (recur (rest bits)
             (+ (* 2 num) next))
      num)))

(defn read-value [bits]
  (loop [size 0
         bits bits
         data []]
    (let [b (first bits)]
      (if (= b 1)
        (recur (+ size 5)
               (drop 5 bits)
               (into data (take 4 (rest bits))))
        {:size (+ size 5)
         :data (make-int (into data (take 4 (rest bits))))}))))

(declare read-packet)

(defn read-operator [bits]
  (let [length-id (first bits)
        bits (rest bits)]
    (if (= length-id 0)
      (let [subpacket-length (make-int (take 15 bits))
            bits             (drop 15 bits)]
        (loop [bits (take subpacket-length bits)
               subpackets []]
          (if (empty? bits)
            {:size (+ 1 15 subpacket-length)
             :data subpackets}
            (let [packet (read-packet bits)]
              (recur (drop (:size packet) bits)
                     (conj subpackets packet))))))
      (let [num-subpackets (make-int (take 11 bits))]
        (loop [bits (drop 11 bits)
               data-size 0
               subpackets []]
          (if (= (count subpackets) num-subpackets)
            {:size (+ 1 11 data-size)
             :data subpackets}
            (let [packet (read-packet bits)]
              (recur (drop (:size packet) bits)
                     (+ data-size (:size packet))
                     (conj subpackets packet)))))))))

(defn read-packet [bits]
  (let [version (make-int (take 3 bits))
        bits    (drop 3 bits)
        id      (make-int (take 3 bits))
        bits    (drop 3 bits)
        header-size    6
        {payload :data
         payload-size :size} (if (= id 4)
                               (read-value bits)
                               (read-operator bits))]
    {:version version
     :id      id
     :size    (+ header-size payload-size)
     :payload payload}))

(defn analyze [packet]
  (if (= (:id packet) 4)
    {:type :value
     :version (:version packet)
     :id      (:id packet)
     :value (:payload packet)}
    {:type :operator
     :id      (:id packet)
     :version (:version packet)
     :operands (mapv analyze (:payload packet))}))

(defn parse [input]
  (-> input
     read-bits
     read-packet
     analyze))

;;; Part 1
(defn run-1 [input]
  (let [packet (parse input)
        packet-seq  (tree-seq map? :operands packet)]
    (->> packet-seq
       (map :version)
       (reduce +))))

(defn run-real-1 []
  (run-1 real-input))

;;; Part 2

(defn eval-packet [packet]
  (let [op-values (map eval-packet (:operands packet))]
    (case (:id packet)
      0 (reduce + op-values)
      1 (reduce * op-values)
      2 (reduce min op-values)
      3 (reduce max op-values)
      4 (:value packet)
      5 (if (> (first op-values) (second op-values)) 1 0)
      6 (if (< (first op-values) (second op-values)) 1 0)
      7 (if (= (first op-values) (second op-values)) 1 0))))

(defn run-2 [input]
  (-> input
     parse
     eval-packet))

(defn run-real-2 []
  (run-2 real-input))
