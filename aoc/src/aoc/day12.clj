(ns aoc.day12
  (:require [clojure.string :as string]))

(def tests ["start-A
start-b
A-c
A-b
b-d
A-end
b-end"
"dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc"
"fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW"])

(def real-input "zs-WO
zs-QJ
WO-zt
zs-DP
WO-end
gv-zt
iu-SK
HW-zs
iu-WO
gv-WO
gv-start
gv-DP
start-WO
HW-zt
iu-HW
gv-HW
zs-SK
HW-end
zs-end
DP-by
DP-iu
zt-start")

(defn parse-line [line]
  (rest (re-find #"([a-zA-Z]+)\-([a-zA-Z]+)" line)))

(defn stuff
  ([acc] acc)
  ([acc [k v]]
   (-> acc
      (update (keyword k) (fnil conj #{}) (keyword v))
      (update (keyword v) (fnil conj #{}) (keyword k)))))

(defn parse [input]
  (let [lines (string/split-lines input)]
    (transduce (map parse-line) stuff {} lines)))

(defn lower-case? [str]
  (= (string/lower-case str) str))

(defn small? [cave]
  (-> cave
     name
     lower-case?))

(defn solve-1 [input]
  (let [graph (parse input)
        paths (loop [stacks [[[:start] graph]]
                     paths  []]
                (if-let [[path graph] (first stacks)]
                  (let [last-place (peek path)]
                    (if (= last-place
                           :end)
                      (recur (rest stacks)
                             (conj paths path))
                      (let [next-places (graph last-place)
                            new-graph   (if (small? last-place)
                                          (dissoc graph last-place)
                                          graph)]
                        (recur
                         (concat (for [place next-places]
                                   [(conj path place) new-graph])
                                 (rest stacks))
                         paths))))
                  paths))]
    (count paths)))

(defn move-into [entered-twice path graph cave]
  (let [last-place (peek path)]
    (cond
      (= :start last-place) [[(conj path cave) (dissoc graph last-place) entered-twice]]
      (= :end last-place) [[(conj path cave) (dissoc graph last-place)   entered-twice]]
      (small? last-place) (if entered-twice
                            [[(conj path cave) (dissoc graph last-place) entered-twice]]
                            [[(conj path cave) (dissoc graph last-place) false]
                             [(conj path cave) graph true]])
      :else [[(conj path cave) graph entered-twice]])))

(defn solve-2 [input]
  (let [graph (parse input)
        paths (loop [stacks [[[:start] graph false]]
                     paths  #{}]
                (if-let [[path graph entered-twice] (first stacks)]
                  (let [last-place (peek path)]
                    (if (= last-place
                           :end)
                      (recur (rest stacks)
                             (conj paths path))
                      (let [next-places (graph last-place)]
                        (recur
                         (concat (mapcat (partial move-into entered-twice path graph) next-places)
                                 (rest stacks))
                         paths))))
                  paths))]
    (count paths)))

;;; Part 1
(def test-solutions-1 [10 19 226])

(defn run-tests-1 []
  (->> (map solve-1 tests)
     (zipmap test-solutions-1)
     (mapv vec)))

(defn run-real-1 []
  (solve-1 real-input))

;;; Part 2
(def test-solutions-2 [36 103 3509])

(defn run-tests-2 []
  (->> (map solve-2 tests)
     (zipmap test-solutions-2)
     (mapv vec)))

(defn run-real-2 []
  (solve-2 real-input))
