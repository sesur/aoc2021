(ns aoc.day17
  (:require [clojure.string :as string]))

;;; NOTE: the problem is underspecified
;;; if the point [1, 0] is in the target area
;;; then its not possible to complete part 1
;;; since there is no highest point that could be reached
;;; any velocity [1, dy] will reach the position [1, 0]
;;; and reach its highest point at dy(dy+1)/2 which is
;;; clearly not bounded.


(def test-input "target area: x=20..30, y=-10..-5")
(def real-input "target area: x=48..70, y=-189..-148")
(def test-input-2 "target area: x=352..377, y=-49..-30")

(defn parse-range [range-str]
  (->> (re-find #"(-?\d+)..(-?\d+)" range-str)
     rest
     (mapv #(Integer/parseInt %))))

(defn parse [input]
  (let [[_ x-range-str y-range-str] (re-find #".*x=(-?\d+\.\.-?\d+), y=(-?\d+\.\.-?\d+)" input)
        x-range (parse-range x-range-str)
        y-range (parse-range y-range-str)]
    {:x-range x-range
     :y-range y-range}))

(defn solve-1 [f q p]

  ;; solve the equation
  ;; -f/2 x^2 + (p - 1/2) x + (p - q)

  (let [b (- p 1/2)
        s2 (- (* b b) (* -2 f (- p q)))]
    (if (< s2 0)
      nil
      (let [s (Math/sqrt s2)]
        [(+ b s)
         (- b s)]))))

(defn int-range-of [[min max]]
  (let [mini (long (Math/ceil min))
        maxi (long (Math/floor max))]
    (when (<= mini maxi)
      [mini maxi])))

(defn solve [f start end val]
  (let [mins (solve-1 f start val)
        maxs (solve-1 f end val)]
    (when (and mins maxs)
      (let [[min1 min2] (sort mins)
            [max1 max2] (sort maxs)

            min1        (max min1 0.0)
;;            max1        (min max1 min1)

            max2        (max max2 0.0)
;;            min2        (min max2 min2)
            ]
        [(int-range-of [max2 min2])
         (int-range-of [min1 max1])]))))

(defn solve-y [ymin ymax dy]
  (solve 1 ymin ymax dy))

(defn find-dx [xmin xmax [start end :as n-range]]
  (when n-range
    (loop [ns (range start (inc end))]
      (when-let [n (first ns)]
        (let [big-s (Math/ceil (max n (+ (/ xmin (inc n)) (/ n 2))))
              big-e (Math/floor (+ (/ xmax (inc n)) (/ n 2)))]
          (if (<= big-s big-e)
            (long big-s)
            (let [s2 (+ 1/4 (* 2 xmin))
                  s  (+ 1/2 (Math/sqrt s2))
                  si (long (Math/ceil s))
                  e2 (+ 1/4 (* 2 xmax))
                  e  (+ 1/2 (Math/sqrt e2))
                  ei (long (Math/floor e))]
              (if (<= si ei)
                  si
                  (recur (rest ns))))))))))

(defn find-all-dx [xmin xmax [start end :as n-range]]
  (when n-range
    (loop [ns (range start (inc end))
           found #{}]
      (if-let [n (first ns)]
        (let [big-s (+ (/ xmin (inc n)) (/ n 2))
              big-e (+ (/ xmax (inc n)) (/ n 2))

              big-si (long (max n (Math/ceil big-s)))
              big-ei (long (Math/floor big-e))

              s2 (+ 1/4 (* 2 xmin))
              s  (- (Math/sqrt s2) 1/2)
              si (long (Math/ceil s))
              e2 (+ 1/4 (* 2 xmax))
              e  (- (Math/sqrt e2) 1/2)
              ei (min n (long (Math/floor e)))]
          (recur (rest ns)
                 (cond-> found
                   (<= big-s big-e) (into (range big-si (inc big-ei)))
                   (<= si ei)       (into (range si    (inc ei))))))
        found))))

(defn y-step [n dy]
       (- (* (inc n) dy)
          (* n (inc n) 1/2)))

(defn x-step [n dx]
  (let [k (min dx n)]
    (- (* (inc k) dx)
       (* k (inc k) 1/2))))

(defn steps [n [dx dy]]
  (map (juxt #(x-step % dx) #(y-step % dy)) (range (inc n))))

;;; Part 1

;; (<= (y-step xmax test-dy) ymax)

(defn run-1 [input]
  (let [target-range (parse input)
        [ymin ymax]  (:y-range target-range)
        [xmin xmax]  (:x-range target-range)]
    ;; [dx = xmax, dy = ymax] is always a solution
    (loop [test-dy (inc ymax)
           dx xmax
           dy ymax]
      (let [new-dy test-dy
            [n-range-1 n-range-2] (solve-y ymin ymax new-dy)]
        ;; if we assume that ymax < 0, we can substitute
        ;; 1000 by (-ymin+1) since the projectile always,
        ;; regardless of dx, reaches the point [_, 0] after 2*dy steps
        ;; with current speed -dy, so the next point will alwyas be
        ;; [_, -dy], so if dy = -ymin + 1, then
        ;; [_, -dy] = [_, -ymin - 1] which cant be in the target
        ;; area anymore
        ;; it might not be possible to give an accurate bound if
        ;; ymax > 0, ymin < 0. And for other reasons (see Note at top).
        ;; so i just chose 1000 since it worked for my input
        (if (< test-dy 1000)
          (if-let [new-dx (or (find-dx xmin xmax n-range-1)
                              (find-dx xmin xmax n-range-2))]
            (recur (inc test-dy)
                   new-dx new-dy)
            (recur (inc test-dy)
                   dx dy))
          ;; this only works assuming dy is positiv but that should be duable
          (y-step dy dy))))))

(defn run-test-1 []
  (run-1 test-input))

(defn run-real-1 []
  (run-1 real-input))

;;; Part 2

(defn pair< [[a b] [x y]]
  (let [c1 (compare a x)
        c2 (compare b y)]
    (if (= c2 0)
      c1
      c2)))

(def test-solution
  (sort pair<
        [[23,-10]  [25,-9]   [27,-5]   [29,-6]   [22,-6]   [21,-7]   [9,0]     [27,-7]   [24,-5]
         [25,-7]   [26,-6]   [25,-5]   [6,8]     [11,-2]   [20,-5]   [29,-10]  [6,3]     [28,-7]
         [8,0]     [30,-6]   [29,-8]   [20,-10]  [6,7]     [6,4]     [6,1]     [14,-4]   [21,-6]
         [26,-10]  [7,-1]    [7,7]     [8,-1]    [21,-9]   [6,2]     [20,-7]   [30,-10]  [14,-3]
         [20,-8]   [13,-2]   [7,3]     [28,-8]   [29,-9]   [15,-3]   [22,-5]   [26,-8]   [25,-8]
         [25,-6]   [15,-4]   [9,-2]    [15,-2]   [12,-2]   [28,-9]   [12,-3]   [24,-6]   [23,-7]
         [25,-10]  [7,8]     [11,-3]   [26,-7]   [7,1]     [23,-9]   [6,0]     [22,-10]  [27,-6]
         [8,1]     [22,-8]   [13,-4]   [7,6]     [28,-6]   [11,-4]   [12,-4]   [26,-9]   [7,4]
         [24,-10]  [23,-8]   [30,-8]   [7,0]     [9,-1]    [10,-1]   [26,-5]   [22,-9]   [6,5]
         [7,5]     [23,-6]   [28,-10]  [10,-2]   [11,-1]   [20,-9]   [14,-2]   [29,-7]   [13,-3]
         [23,-5]   [24,-8]   [27,-9]   [30,-7]   [28,-5]   [21,-10]  [7,9]     [6,6]     [21,-5]
         [27,-10]  [7,2]     [30,-9]   [21,-8]   [22,-7]   [24,-9]   [20,-6]   [6,9]     [29,-5]
         [8,-2]    [27,-8]   [30,-5]   [24,-7]]))

(defn print-difference [c1 c2]
  (loop [c1 c1
         c2 c2]
    (when (or (seq c1) (seq c2))
      (let [e1 (first c1)
            e2 (first c2)]
        (cond
          (and (nil? e1) (nil? e2)) nil
          (= e1 e2)     (do
                          (println (format "%10s %10s" (str e1) (str e2)))
                          (recur (rest c1) (rest c2)))

          (nil? e1)     (do
                          (println (format "%10s %10s" "----------" (str e2)))
                          (recur c1 (rest c2)))
          (nil? e2)     (do
                          (println (format "%10s %10s" (str e1) "----------"))
                          (recur (rest c1) c2))

          (= -1 (pair< e1 e2)) (do
                          (println (format "%10s %10s" (str e1) "----------"))
                          (recur (rest c1) c2))
          :else         (do
                          (println (format "%10s %10s" "----------" (str e2)))
                          (recur c1 (rest c2))))))))

(defn run-2 [input]
  (let [target-range (parse input)
        [ymin ymax]  (:y-range target-range)
        [xmin xmax]  (:x-range target-range)]
    (loop [test-dy ymin
           possible #{}]
      (let [new-dy test-dy
            [n-range-1 n-range-2] (solve-y ymin ymax new-dy)]
        ;; the real bound would be the solution from part 1
        (if (< test-dy 1000)
          (if-let [new-dxs (concat (find-all-dx xmin xmax n-range-1)
                                   (find-all-dx xmin xmax n-range-2))]
            (recur (inc test-dy)
                   (into possible (map #(vector % new-dy) new-dxs)))
            (recur (inc test-dy)
                   possible))
          (sort pair< possible))))))

(defn run-test-2 []
  (run-2 test-input))

(defn run-real-2 []
  (count (run-2 real-input)))
