(ns aos.day14
  (:require [clojure.string :as string]))

(def test-input "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C")

(def real-input "BSONBHNSSCFPSFOPHKPK

PF -> P
KO -> H
CH -> K
KN -> S
SS -> K
KB -> B
VS -> V
KV -> O
KP -> B
OF -> C
HB -> C
NP -> O
NS -> V
VO -> P
VF -> H
CK -> B
PC -> O
SK -> O
KF -> H
FV -> V
PP -> H
KS -> B
FP -> N
BV -> V
SB -> F
PB -> B
ON -> F
SF -> P
VH -> F
FC -> N
CB -> H
HP -> B
NC -> B
FH -> K
BF -> P
CN -> N
NK -> H
SC -> S
PK -> V
PV -> C
KC -> H
HN -> K
NO -> H
NN -> S
VC -> P
FF -> N
OO -> H
BK -> N
FS -> V
BO -> F
SH -> S
VK -> F
OC -> F
FN -> V
OV -> K
CF -> F
NV -> V
OP -> K
PN -> K
SO -> P
PS -> S
KK -> H
HH -> K
NH -> O
FB -> K
HS -> B
BB -> V
VB -> O
BH -> H
OK -> C
CC -> B
FK -> N
SN -> V
HK -> N
KH -> F
OS -> O
FO -> P
OH -> B
CP -> S
BN -> H
OB -> B
BP -> B
CO -> K
SP -> K
BS -> P
VV -> N
VN -> O
NF -> F
CV -> B
HC -> B
HV -> S
BC -> O
HO -> H
PO -> P
CS -> B
PH -> S
SV -> V
VP -> C
NB -> K
HF -> C")

(defn parse-rule [rule-str]
  (let [[_ input output] (re-find #"([a-zA-Z]{2}) -> ([a-zA-Z])" rule-str)]
    [[(first input) (second input)] (first output)]))

(defn parse [input]
  (let [[start-str rules-str] (string/split input #"\n\n")]
    {:start (->> start-str
               (partition 2 1)
               (map vec)
               frequencies)
     :rules (into {} (map parse-rule (string/split-lines rules-str)))}))

(defn score-1 [state]
  (let [proto-scores (reduce (fn [acc [[a b] num]]
                               (-> acc
                                  (update a (fnil + 0) (/ num 2))
                                  (update b (fnil + 0) (/ num 2)))) {} state)
        ;; every element of the string is counted twice except for the
        ;; first and last ones

        real-scores (reduce-kv (fn [acc a num]
                                 (if (integer? num)
                                   (assoc acc a num)
                                   (assoc acc a (+ 1/2 num)))){} proto-scores)
        scores      (sort (vals real-scores))
        min         (first scores)
        max         (last  scores)]
    (- max min)))

;;; Part 1

(defn run-1 [N input]
  (let [{:keys [start rules]} (parse input)]
    (loop [cnt 0
           current start]
      (if (< cnt N)
        (let [new (reduce (fn [acc [[a c :as pair] num]]
                            (let [b (rules pair)]
                              (if b
                                (-> acc
                                   (update [a b] (fnil + 0) num)
                                   (update [b c] (fnil + 0) num))
                                (update acc pair (fnil + 0) num))))
                          {}
                          current)]
          (recur (inc cnt) new))
        (score-1 current)))))

(defn run-test-1 []
  (run-1 10 test-input))

(defn run-real-1 []
  (run-1 10 real-input))

;;; Part 2

(defn run-test-2 []
  (run-1 40 test-input))

(defn run-real-2 []
  (run-1 40 real-input))
