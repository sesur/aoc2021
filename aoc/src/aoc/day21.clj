(ns aoc.day21
  (:require [clojure.string :as string]))

(def test-input "Player 1 starting position: 4
Player 2 starting position: 8")

(def real-input "Player 1 starting position: 8
Player 2 starting position: 1")

(defn parse-num [c]
  (let [ret (Long/parseLong (str c))]
    (if (= ret 0)
      10
      ret)))

(defn parse [input]
  (let [[p1 p2] (string/split-lines input)]
    {:player-1 (parse-num (last p1))
     :player-2 (parse-num (last p2))}))
