(ns aoc.day24
  (:require [clojure.string :as string]))

(def real-input "inp w
mul x 0
add x z
mod x 26
div z 1
add x 15
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 13
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 10
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 16
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 12
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 2
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 10
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 8
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 14
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 11
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -11
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 6
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 10
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 12
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -16
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 2
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -9
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 2
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 1
add x 11
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 15
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -8
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 1
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -8
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 10
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -10
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 14
mul y x
add z y
inp w
mul x 0
add x z
mod x 26
div z 26
add x -9
eql x w
eql x 0
mul y 0
add y 25
mul y x
add y 1
mul z y
mul y 0
add y w
add y 10
mul y x
add z y")

(def test-inputs
  ["inp x
mul x -1"
   "inp z
inp x
mul z 3
eql z x"
   "inp w
add z w
mod z 2
div w 2
add y w
mod y 2
div w 2
add x w
mod x 2
div w 2
mod w 2"])

(defn parse-instr [str]
  (keyword str))
(defn parse-param [str]
  (if-let [[_ num-str] (re-find #"(-?\d+)" str)]
    (Long/parseLong num-str)
    (keyword str)))

(defn parse-instruction [line]
  (let [instr-str (subs line 0 3)
        param-str     (subs line 4)]
    {:instr (parse-instr instr-str)
     :params (mapv parse-param (string/split param-str #"\s"))}))
(defn parse [input]
  (let [lines (string/split-lines input)]
    (mapv parse-instruction lines)))

(defn input? [val]
  (and
   (seq? val)
   (= (first val) 'nth)))

(defn with-substitutions [subs expr]
  (if (not (seq? expr))
    expr
    (if-let [sub (subs expr)]
      sub
      (let [[instr & params] expr]
        (conj (map #(with-substitutions subs %) params)
              instr)))))

(defn add-substitution [subs expr]
  (if (not (seq? expr))
    [subs expr]
    (if-let [sub (subs expr)]
      [subs sub]
      (let [[instr & params] expr
            param-list (transient [])
            new-subs (reduce (fn [subs param]
                               (let [[new-subs sub] (add-substitution subs param)]
                                 (conj! param-list sub)
                                 new-subs)) subs params)
            next (:max new-subs 0)
            next-sub (keyword (str "let-" next))
            sub-expr (conj (seq (persistent! param-list)) instr)]
        [(-> new-subs
             (assoc :max (inc next))
             (assoc sub-expr next-sub)
             (assoc next-sub sub-expr))
         next-sub]))))

(defn if-expr? [expr]
  (and (seq? expr)
       (= (first expr) 'if)))

(defn cmp [a b]
  (if (= a b)
    1
    0))

(defn advance-state [state instr op1 op2]
  (let [val1 (get state op1)
        ;; op2 might be a constant
        val2 (get state op2 op2)]
    (case instr
      :add (cond
             (= val2 0)                   state
             (= val1 0)                   (assoc state op1 val2)
             (and (integer? val1) (integer? val2)) (assoc state op1 (+ val1 val2))
             ;; (or (if-expr? val1)
             ;;     (and (keyword? val1)
             ;;          (if-expr? ((:subs state {}) val1))))
             ;; (let [if-expr (if (keyword? val1) ((:subs state {}) val1) val1)
             ;;       [_ pred then else] if-expr
             ;;       new-then (if (and (integer? then)
             ;;                         (integer? val2))
             ;;                  (+ then val2)
             ;;                  `(+ ~then ~val2))
             ;;       new-else (if (and (integer? else)
             ;;                         (integer? val2))
             ;;                  (+ else val2)
             ;;                  `(+ ~else ~val2))
             ;;       expr `(if ~pred ~new-then ~new-else)
             ;;       [new-subs sub] (add-substitution (:subs state {}) expr)]
             ;;   (-> state
             ;;      (assoc op1 sub)
             ;;      (assoc :subs new-subs)))
             ;; (or (if-expr? val2)
             ;;     (and (keyword? val2)
             ;;          (if-expr? ((:subs state {}) val2))))
             ;; (let [if-expr (if (keyword? val2) ((:subs state {}) val2) val2)
             ;;       [_ pred then else] if-expr
             ;;       new-then (if (and (integer? then)
             ;;                         (integer? val2))
             ;;                  (+ then val2)
             ;;                  `(+ ~then ~val2))
             ;;       new-else (if (and (integer? else)
             ;;                         (integer? val2))
             ;;                  (+ else val2)
             ;;                  `(+ ~else ~val2))
             ;;       expr `(if ~pred ~new-then ~new-else)
             ;;       [new-subs sub] (add-substitution (:subs state {}) expr)]
             ;;   (-> state
             ;;      (assoc op1 sub)
             ;;      (assoc :subs new-subs)))
             :else (let [expr `(+ ~val1 ~val2)
                         [new-subs sub] (add-substitution (:subs state {}) expr)]
                     (-> state
                        (assoc op1 sub)
                        (assoc :subs new-subs))))
      :mul (cond
             (= 0 val1)                   state
             (= 1 val1)                   (assoc state op1 val2)
             (= 0 val2)                   (assoc state op1 0)
             (= 1 val2)                   state
             (and (integer? val1) (integer? val2)) (assoc state op1 (* val1 val2))
             (or (if-expr? val1)
                 (and (keyword? val1)
                      (if-expr? ((:subs state {}) val1))))
             (let [if-expr (if (keyword? val1) ((:subs state {}) val1) val1)
                   [_ pred then else] if-expr
                   new-then (cond (= 0 then) 0 (= 1 then) val2 :else `(* ~then ~val2))
                   new-else (cond (= 0 else) 0 (= 1 else) val2 :else `(* ~else ~val2))
                   expr `(if ~pred ~new-then ~new-else)
                   [new-subs sub] (add-substitution (:subs state {}) expr)]
               (-> state
                  (assoc op1 sub)
                  (assoc :subs new-subs)))
             (or (if-expr? val2)
                 (and (keyword? val2)
                      (if-expr? ((:subs state {}) val2))))
             (let [if-expr (if (keyword? val2) ((:subs state {}) val2) val2)
                   [_ pred then else] if-expr
                   new-then (cond (= 0 then) 0 (= 1 then) val1 :else `(* ~then ~val1))
                   new-else (cond (= 0 else) 0 (= 1 else) val1 :else `(* ~else ~val1))
                   expr `(if ~pred ~new-then ~new-else)
                   [new-subs sub] (add-substitution (:subs state {}) expr)]
               (-> state
                  (assoc op1 sub)
                  (assoc :subs new-subs)))
             :else (let [expr `(* ~val1 ~val2)
                         [new-subs sub] (add-substitution (:subs state {}) expr)]
                     (-> state
                        (assoc op1 sub)
                        (assoc :subs new-subs))))
      :div (cond
             (= 0 val1)                   state
             (= 1 val2)                   state
             (and (integer? val1) (integer? val2)) (assoc state op1 (quot val1 val2))
             :else (let [expr `(quot ~val1 ~val2)
                         [new-subs sub] (add-substitution (:subs state {}) expr)]
                     (-> state
                        (assoc op1 sub)
                        (assoc :subs new-subs))))
      :mod (cond
             (= 0 val1)                   state
             (= 1 val2)                   state
             (and (integer? val1) (integer? val2)) (assoc state op1 (mod val1 val2))
             :else (let [expr `(mod ~val1 ~val2)
                         [new-subs sub] (add-substitution (:subs state {}) expr)]
                     (-> state
                        (assoc op1 sub)
                        (assoc :subs new-subs))))
      :eql (cond
             (or (and (input? val1)
                      (integer? val2)
                      (or (< val2 1)
                          (> val2 9)))
                 (and (input? val2)
                      (integer? val1)
                      (or (< val1 1)
                          (> val1 9))))   (assoc state op1 0)
             (and (integer? val1) (integer? val2)) (assoc state op1 (if (= val1 val2) 1 0))
             :else (if (and (integer? val2)
                            (or (if-expr? val1)
                                (and (keyword? val1)
                                     (if-expr? ((:subs state {}) val1)))))
                     (case val2
                       1 (assoc state op1 (add-substitution (:subs state {}) val1))
                       0 (let [if-expr (if (keyword? val1) ((:subs state {}) val1) val1)
                               [_ pred then else] if-expr
                               expr `(if ~pred ~else ~then)
                               [new-subs sub] (add-substitution (:subs state {}) expr)]
                           (-> state
                               (assoc op1 sub)
                               (assoc :subs new-subs)))
                       (assoc state op1 0))

                     (let [expr `(if (= ~val1 ~val2) 1 0)
                           [new-subs sub] (add-substitution (:subs state {}) expr)]
                       (-> state
                           (assoc op1 sub)
                           (assoc :subs new-subs))))))))

(defn execute [instructions input]
  (loop [instructions instructions
         input        input
         state        {:x 0 :y 0 :z 0 :w 0}]
    (if-let [{instr :instr
              [op1 op2] :params} (first instructions)]
      (case instr
        :inp (recur (rest instructions)
                    (rest input)
                    (assoc state op1 (first input)))
        (recur (rest instructions)
               input
               (advance-state state instr op1 op2)))
      state)))

(defn reverse-map [m]
  (reduce-kv (fn [acc k v]
               (assoc acc v k)) {} m))

(defn let-num [letter]
  (Long/parseLong (subs (name letter) 4)))

(defn let->val [expr]
  (cond (keyword? expr) (symbol (str "val-" (let-num expr)))
        (seq?     expr) (map let->val expr)
        :else           expr))

(defn build-let-chain [rsubs]
  (let [keys (keys rsubs)
        lets (sort-by let-num keys)]
    (into [] (comp
              (map (juxt let-num rsubs))
              (map (fn [[num expr]]
                     [(symbol (str "val-" num))
                      (let->val expr)]))) lets)))

(defn dependencies [rsubs]
  (reduce-kv (fn [acc name expr]
               (let [deps (filter keyword? (flatten expr))]
                 (reduce (fn [acc dep]
                           (update acc dep (fnil conj []) name)) acc deps))) {}
             rsubs))

(defn deep-replace [rmap expr]
  (if (seq? expr)
    (map (partial deep-replace rmap) expr)
    (rmap expr expr)))

(defn kill-single-deps
  ([dependencies rsubs val] (kill-single-deps dependencies rsubs val (constantly true) count))
  ([dependencies rsubs val should-replace num-deps]
   (reduce (fn [rsubs letter]
             (cond
               (and
                (not= val letter)
                (= (num-deps (dependencies letter)) 0)) (dissoc rsubs letter)
               (= (num-deps (dependencies letter)) 1) (let [dependant (first (dependencies letter))
                                                            dep-value (rsubs dependant)
                                                            value     (rsubs letter)]

                                                     (if (should-replace letter value dependant dep-value)
                                                       (-> rsubs
                                                          (dissoc letter)
                                                          (update dependant (partial deep-replace {letter value})))
                                                       rsubs))
               :else rsubs)) rsubs (sort-by let-num (keys rsubs)))))

(defn with-arguments
  ([expr] (second (with-arguments 0 expr)))
  ([n expr]
   (cond
     (seq? expr) (let [[new-n new-e] (reduce (fn [[current-n acc] e]
                                               (let [[new-n new-e] (with-arguments current-n e)]
                                                 [new-n (conj acc new-e)])) [n []] expr)]
                   [new-n (seq new-e)])
     (or (integer? expr)
         (keyword? expr))  [(inc n) (symbol (str "arg-" n))]
     :else                 [n expr])))

(defn pred-or [pred-1 pred-2]
  (fn [x]
    (or (pred-1 x)
        (pred-2 x))))

(defn extract-function [expr]
  (let [arguments  (filter (pred-or keyword? integer?) (flatten expr))
        num-args   (count arguments)
        dummy-args (map #(symbol (str "arg-" %)) (range))]
    {:fn `(fn ~(vec (take num-args dummy-args))
            ~(with-arguments expr))
     :num-arguments num-args
     :args arguments}))



(defn find-constants [arg-lists]
  (let [positioned (map (partial zipmap (range)) (map second arg-lists))]
    (reduce (partial merge-with (fn [a b] (when (= a b) a))) positioned)))

(defn with-constants [[_ arg-list & body] var-positions constants]
  (let [const-args (reduce-kv (fn [acc k v]
                                (assoc acc (symbol (str "arg-" k)) v)) {} constants)]
    `(fn ~(mapv #(nth arg-list %) var-positions)
       ~@(deep-replace const-args body))))

(defn find-same [positions arg-lists]
  (let [positioned (map (partial zipmap (range)) (map second arg-lists))
        indices (for [a positions
                      b positions
                      :when (< a b)]
                  [a b])]
    (reduce (fn [same [a b]]
              (if (= (map #(get % a) positioned)
                     (map #(get % b) positioned))
                (assoc same (same b b) (same a a))
                same)) {} indices)))

(defn with-same [[_ arg-list & body] same]
  (let [rmap (reduce-kv (fn [acc k v]
                          (assoc acc (symbol (str "arg-" k))
                                 (symbol (str "arg-" v)))) {} same)]
    `(fn ~(vec (remove (into #{} (keys rmap)) arg-list))
       ~@(deep-replace rmap body))))

(defn use-function [fun-name locations rfun-subs used-at]
  (let [constants (find-constants used-at)
        const-positions (reduce-kv (fn [acc k v]
                                     (if v
                                       (conj acc k)
                                       acc)) [] constants)
        var-positions   (reduce-kv (fn [acc k v]
                                     (if v
                                       acc
                                       (conj acc k))) [] constants)
        same            (find-same var-positions used-at)
        var-positions   (vec (remove (into #{} (keys same)) var-positions))
        constants (select-keys constants const-positions)]
    [(reduce (fn [acc [value args]]
               (assoc acc value [fun-name (map #(nth args %) var-positions)]))
             locations used-at)
     (-> rfun-subs
        (update fun-name with-constants var-positions constants)
        (update fun-name with-same      same))]))

(defn normalise [rsubs val]
  (let [no-single-deps (loop [rsubs rsubs]
                         (let [deps (dependencies rsubs)]
                           (if (some #(< (count %) 2) (vals deps))
                             (let [new-rsubs (kill-single-deps deps rsubs val)]
                               (if (not= new-rsubs rsubs)
                                 (recur new-rsubs)
                                 new-rsubs))
                             rsubs)))
        [replacements rfun-subs] (loop [fun-subs {}
                                        used-at  {}
                                        to-go (seq no-single-deps)]
                                   (if-let [[val expr] (first to-go)]
                                     (let [{:keys [fn num-arguments args]} (extract-function expr)]
                                       (if (= 0 num-arguments)
                                         (recur
                                          fun-subs
                                          used-at
                                          (rest to-go))
                                         (if-let [sub (fun-subs fn)]
                                           (recur
                                            fun-subs
                                            (update used-at sub conj [val args])
                                            (rest to-go))
                                           (let [num (count fun-subs)
                                                 sub-name (symbol (str "fun-" num))]
                                             (recur (assoc fun-subs fn sub-name)
                                                    (assoc used-at sub-name [[val args]])
                                                    (rest to-go))))))
                                     (let [rfun-subs (reverse-map fun-subs)]
                                       (reduce (fn [[locations rfun-subs] [fun-name used-at]]
                                                 (if (> (count used-at) 1)
                                                   (use-function fun-name locations rfun-subs used-at)
                                                   [locations (dissoc rfun-subs fun-name)]))
                                               [{} rfun-subs] used-at))))]

    (assoc
     (reduce
      (fn [rsubs name]
        (if-let [[fun args] (replacements name)]
          (assoc rsubs name (conj args fun))
          rsubs))
      no-single-deps (keys no-single-deps))
     :fun-subs
     rfun-subs)))

(defn rename [rsubs val]
  (loop [counter 0
         renames {}
         new-rsubs   {}
         to-go   (sort-by (comp let-num first) (seq rsubs))]
    (if-let [[name expr] (first to-go)]
      (let [new-name (keyword (str "let-" counter))]
        (recur
         (inc counter)
         (assoc renames name new-name)
         (assoc new-rsubs new-name (replace renames expr))
         (rest to-go)))
      [(renames val val) new-rsubs])))

(defn should-replace [_ [fun-1 & args-1] _ [fun-2 & args-2]]
  (#{'[fun-0 fun-1]
     '[fun-0 fun-2]
     '[fun-0 fun-3]
     '[fun-1 fun-2]
     '[fun-1 fun-3]} [fun-1 fun-2])
  #_(when (and
       (symbol? fun-1)
       (symbol? fun-2)
       (string/starts-with? (name fun-1) "fun-")
       (string/starts-with? (name fun-2) "fun-"))
      (let [num-1 (Long/parseLong (subs (name fun-1) (count "fun-")))
            num-2 (Long/parseLong (subs (name fun-2) (count "fun-")))]
        (< num-1 num-2))))

(defn second-normalise [normalised val]

  (let [old-fun-subs       (:fun-subs normalised)
        num-old            (count old-fun-subs)
        no-single-deps (loop [rsubs (dissoc normalised :fun-subs)]
                         (let [deps (dependencies rsubs)]
                           (if (some #(< (count (set %)) 2) (vals deps))
                             (let [new-rsubs (kill-single-deps deps rsubs val should-replace #(count (set %)))]
                               (if (not= new-rsubs rsubs)
                                 (recur new-rsubs)
                                 new-rsubs))
                             rsubs)))
        [replacements rfun-subs] (loop [fun-subs {}
                                        used-at  {}
                                        to-go (seq no-single-deps)]
                                   (if-let [[val expr] (first to-go)]
                                     (let [{:keys [fn num-arguments args]} (extract-function expr)]
                                       (if (= 0 num-arguments)
                                         (recur
                                          fun-subs
                                          used-at
                                          (rest to-go))
                                         (if-let [sub (fun-subs fn)]
                                           (recur
                                            fun-subs
                                            (update used-at sub conj [val args])
                                            (rest to-go))
                                           (let [num (+ num-old (count fun-subs))
                                                 sub-name (symbol (str "fun-" num))]
                                             (recur (assoc fun-subs fn sub-name)
                                                    (assoc used-at sub-name [[val args]])
                                                    (rest to-go))))))
                                     (let [rfun-subs (reverse-map fun-subs)]
                                       (reduce (fn [[locations rfun-subs] [fun-name used-at]]
                                                 (if (> (count used-at) 1)
                                                   (use-function fun-name locations rfun-subs used-at)
                                                   [locations (dissoc rfun-subs fun-name)]))
                                               [{} rfun-subs] used-at))))]
    (assoc
     (reduce
      (fn [rsubs name]
        (if-let [[fun args] (replacements name)]
          (assoc rsubs name (conj args fun))
          rsubs))
      no-single-deps (keys no-single-deps))
     :fun-subs
     (merge old-fun-subs rfun-subs))))

(defn simplify-function [fun-subs fun]
  (if (seq? fun)
    (let [[fname & fargs] fun]
      (if (string/starts-with? (name fname) "fun-")
        (let [new-args      (map (partial simplify-function fun-subs) fargs)
              params        (map #(symbol (str "param-" %)) (range (count fargs)))
              [_ input-args body] (fun-subs fname)]
          (deep-replace (zipmap input-args new-args) body))
        fun))
    fun))

(defn simplify [fun-subs]
  (reduce-kv (fn [acc k v]
               (let [[_ args body] v]
                 (assoc acc k `(fn ~args ~(simplify-function fun-subs body))))) {} fun-subs))

(defn clear-up [state register]
  (let [subs (:subs state)
        val  (state register)
        rsubs (reverse-map (select-keys subs (remove keyword? (keys subs))))

        normalised   (normalise rsubs val)

        normalised   (second-normalise normalised val)

        fun-subs     (simplify (:fun-subs normalised))

        normalised   (dissoc normalised :fun-subs)

        used-functions (into #{} (map first (vals normalised)))

        [new-val normalised]    (rename normalised val)]
    {:funs  (select-keys fun-subs used-functions)
     :steps normalised
     :start new-val}))

(defn build-expression [{:keys [funs steps start]}]
  (if (keyword? start)
    `(let ~(into (vec (apply concat funs)) (mapcat identity) (build-let-chain steps))
       ~(symbol (str "val-" (let-num start))))
    start))

(defn build-fn [stuff]
  (eval `(fn [~'input]
           ~(build-expression stuff))))

;;; Part 1

(defn try-equal [inputs])

(defn run-1 []
  (let [generic-inputs (map (fn [x] (list 'nth 'input x)) (range))
        generic-result (execute (parse real-input) generic-inputs)]
    (clear-up generic-result :z)))

(defn abstract-input [i]
  (mapv (fn [v] (assoc (vec (repeat 14 nil)) i v)) (range 1 10)))

(defn compatible-position? [a b]
  (or (nil? a)
      (nil? b)
      (= a b)))

(defn compatible? [a b]
  (every? (fn [[a b]] (compatible-position? a b)) (map vector a b)))

(defn combine-position [a b]
  (cond
    (nil? a) b
    (nil? b) a
    (= a b)  a
    :else    :error))

(defn combine [a b]
  (mapv combine-position a b))

(comment
  (def inputs (map (fn [x] (list 'nth 'input x)) (range)))
  (def res (execute (parse real-input) inputs))
  (assert (= ((build-fn res :z) (repeat 14 9)) 10500769))
  (build-expression res :z))
