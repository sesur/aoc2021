(ns aoc.day23
  (:require [clojure.string :as string]
            [clojure.set :as set]))

(def real-input "#############
#...........#
###D#A#C#C###
  #D#A#B#B#
  #########")

(def test-input "#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########")

(def replacements {\. :empty
                   \A :amber
                   \B :bronze
                   \C :copper
                   \D :desert})

(def amphipods [:amber :bronze :copper :desert])

(defn make-neighbors [rooms floor]
  (let [room-xs (set (mapv first rooms))
        room-ys (mapv second rooms)
        floor-xs (mapv first floor)
        minx     (reduce min floor-xs)
        maxx     (reduce max floor-xs)
        miny     (reduce min room-ys)
        maxy     (reduce max room-ys)

        floor-adjs (mapv (fn [[x y]]
                           (cond-> []
                             (not= x minx)     (into (if (room-xs (dec x))
                                                       [[(dec x) (inc y)]
                                                        [(dec (dec x)) y]]
                                                       [[(dec x) y]]))
                             (not= x maxx)     (into (if (room-xs (inc x))
                                                       [[(inc x) (inc y)]
                                                        [(inc (inc x)) y]]
                                                       [[(inc x) y]])))) floor)
        room-adjs (mapv (fn [[x y]]
                          (cond-> []
                            (= y miny)     (into [[x (inc y)] [(dec x) (dec y)]
                                                  [(inc x) (dec y)]])
                            (= y maxy)     (conj [x (dec y)]))) rooms)]
    (merge (zipmap rooms room-adjs)
           (zipmap floor floor-adjs))))

(defn parse [input]
  (let [numbered (map-indexed (fn [row line]
                                (map-indexed (fn [column char]
                                               [char column row]) line)) (string/split-lines input))
        spaces (reduce (fn [acc [char c r]]
                         (if-let [rep (replacements char)]
                           (conj acc [rep c r])
                           acc)) [] (apply concat numbered))
        room-spaces (remove #(= (first %) :empty) spaces)
        floor-spaces (mapv #(subvec % 1) (filter #(= (first %) :empty) spaces))
        rooms       (reduce-kv (fn [acc k v]
                                 (assoc acc k (ffirst v)))
                               {} (group-by #(subvec % 1) room-spaces))
        positions   (reduce (fn [acc [amphi x y]]
                              (conj acc {:amphi amphi :pos [x y]})) [] room-spaces)]
    {:preferred-rooms (zipmap amphipods (mapv set (map second (sort-by first (group-by first (keys rooms))))))
     ;;:spaces (merge (zipmap floor-spaces (repeat nil)) rooms)
     :rooms  (set (keys rooms))
     :positions positions
     :floor-positions (set (remove #((set (map first (keys rooms))) (first %)) floor-spaces))
     ;;:neighbors (make-neighbors (keys rooms) floor-spaces)
     }))

(defn unoccupied? [spaces a]
  (nil? (spaces a)))

(defn steps [[x1 y1] [x2 y2]]
  (+ (Math/abs (- x2 x1))
     (Math/abs (- y2 y1))))

(def energy-per-step {:amber  1,,,
                      :bronze 10,,
                      :copper 100,
                      :desert 1000})

(defn heuristic [preferred-rooms positions]
  ;; assuming floor is at y = 1
  (reduce (fn [acc {:keys [amphi pos]}]
            (let [[x y] pos
                  [[x1 y1] [x2 y2]] (vec (preferred-rooms amphi))]
              (+ acc (min (* (energy-per-step amphi)
                             (+ (if (= x1 x)
                                  0
                                  (+ (dec y)
                                     (dec y1)))
                                (Math/abs (- x1 x))))
                          (* (energy-per-step amphi)
                             (+ (if (= x2 x)
                                  0
                                  (+ (dec y2) (dec y)))
                                (Math/abs (- x2 x)))))))) 0 positions))

(defn solved? [preferred-rooms positions]
  (every? (fn [{:keys [amphi pos]}]
            ((preferred-rooms amphi) pos)) positions))

(defn add-to-set [v x]
  (if v
    (conj v x)
    #{x}))

(defn path-of [[x1 y1] [x2 y2]]
  (cond
    (and (= x1 x2)
         (= y1 y2)) nil
    (= x1 x2) [[x2 y2]]
    :else (let [[one-past direction] (if (< x1 x2)
                                       [(inc x2) +1]
                                       [(dec x2) -1])]
            (-> []
               (into (rest (map #(vector x1 %) (range y1 0 -1))))
               (into (rest (map #(vector % 1)  (range x1 one-past direction))))
               (into (rest (map #(vector x2 %) (range 1 (inc y2)))))))))

(defn reachable? [occupied start end]
  (let [path (path-of start end)]
    (not (and path
              (some occupied path)))))

(defn compare-positions [{amphi1 :amphi pos1 :pos} {amphi2 :amphi pos2 :pos}]
  (let [cmpp (compare pos1 pos2)
        cmpa (compare amphi1 amphi2)]
    (if (= 0 cmpp)
      cmpa
      cmpp)))

(defn next-possible' [energy-spent-so-far preferred-rooms rooms
                      floor-positions occupied positions num]
  (let [{:keys [amphi pos]} (nth positions num)
        my-preferred-rooms (preferred-rooms amphi)
        neighbors (filter (partial reachable? occupied pos)
                          (if (contains? rooms pos)
                            ;; inside room
                            (let [preferred (vec (sort my-preferred-rooms))]
                              (if-let [pref-idx (some (fn [n]
                                                        (let [x (nth preferred n)]
                                                          (when (= x pos)
                                                            n))) (range (count preferred)))]
                                (when-not (every? #{amphi} (map (comp occupied #(nth preferred %))
                                                                (range (inc pref-idx) (count preferred))))
                                  ;; inside preferred room, but bad amphis are under me
                                  floor-positions)
                                ;; not inside preferred room
                                (let [preferred    (sort my-preferred-rooms)
                                      empty-count  (count (take-while #(nil? (occupied %)) preferred))
                                      bad-amphis           (->> preferred
                                                         (drop empty-count)
                                                         (drop-while #(= amphi (occupied %))))]
                                  (if (seq bad-amphis)
                                    floor-positions
                                    #{(nth preferred (dec empty-count))}))))
                            ;; on floor
                            (let [preferred    (sort my-preferred-rooms)
                                  empty-count  (count (take-while #(nil? (occupied %)) preferred))
                                  bad-amphis           (->> preferred
                                                     (drop empty-count)
                                                     (drop-while #(= amphi (occupied %))))]
                              (when-not (seq bad-amphis)
                                #{(nth preferred (dec empty-count))}))))]

    (mapv (fn [new-pos]
            (let [energy-spent (+ energy-spent-so-far
                                  (* (count (path-of pos new-pos))
                                     (energy-per-step amphi)))
                  new-positions (assoc-in positions [num :pos] new-pos)
                  heuristic    (heuristic preferred-rooms new-positions)]
              {:previous     positions
               :energy-spent energy-spent
               :guessed-cost (+ energy-spent heuristic)
               :positions    (vec (sort compare-positions new-positions))})) neighbors)))

(defn next-possible [energy-spent-so-far preferred-rooms rooms floor-positions positions]
  (let [occupied (zipmap (map :pos positions) (map :amphi positions))]
    (mapcat (partial next-possible' energy-spent-so-far preferred-rooms rooms floor-positions occupied positions)
            (range (count  positions)))))

(defn better [best-possible current]
  (if-let [current-best (best-possible (set (:position current)))]
    (< (:energy-spent current) current-best)
    true))

(def amphi-char {:bronze \B,
                 :copper \C,
                 :desert \D
                 :amber \A})

(defn draw-small [positions]
  (let [positions (reduce (fn [acc {:keys [amphi pos]}]
                            (assoc acc pos (amphi-char amphi))){} positions)]
    (println "#############")
    (print "#")
    (doseq [x (range 1 12)]
      (print (positions [x 1] ".")))
    (println "#")
    (print "###") (doseq [x (range 4)]
                    (print (str (positions [(+ (* 2 x) 3) 2] ".") "#"))) (println "##")
    (print "  #") (doseq [x (range 4)]
                    (print (str (positions [(+ (* 2 x) 3) 3] ".") "#"))) (println "  ")
    (println "  #########  ")))

(defn draw-big [positions]
  (let [positions (reduce (fn [acc {:keys [amphi pos]}]
                            (assoc acc pos (amphi-char amphi))){} positions)]
    (println "#############")
    (print "#")
    (doseq [x (range 1 12)]
      (print (positions [x 1] ".")))
    (println "#")
    (print "###") (doseq [x (range 4)]
                    (print (str (positions [(+ (* 2 x) 3) 2] ".") "#"))) (println "##")
    (print "  #") (doseq [x (range 4)]
                    (print (str (positions [(+ (* 2 x) 3) 3] ".") "#"))) (println "  ")
    (print "  #") (doseq [x (range 4)]
                    (print (str (positions [(+ (* 2 x) 3) 4] ".") "#"))) (println "  ")
    (print "  #") (doseq [x (range 4)]
                    (print (str (positions [(+ (* 2 x) 3) 5] ".") "#"))) (println "  ")
    (println "  #########  ")))

(def ^:dynamic draw draw-small)

(defn remove-current [possibilities energy current]
  (let [c (count (possibilities energy))]
    (if (= c 1)
      (dissoc possibilities energy)
      (update possibilities energy disj current))))

(defn better [best-possible current]
  (if-let [current-best (best-possible (set (:position current)))]
    (< (:energy-spent current) (:score current-best))
    true))

(defn get-path [best-possible current]
  (loop [path [(:positions current)]
         current (:previous current)]
    (if-let [next (best-possible (set current))]
      (recur (conj path current) (:previous next))
      path)))

(defn find-best [preferred-rooms rooms positions floor-positions]
  (loop [cntr          0
         best-possible {}
         possibilities (sorted-map 0 #{{:energy-spent 0
                                        :guessed-cost 0 ; not right but doesnt matter
                                        :positions positions}})]
    (when-let [[energy states] (first possibilities)]
      (if-let [{:keys [positions energy-spent guessed-cost previous] :as current} (first states)]
        (do
          (when (= (mod cntr 10000) 0)
                                        ;              (println (reduce min (keys possibilities)) (take 100 (keys possibilities)))
            (println "counter" cntr)
            (println "guessed cost:" guessed-cost)
            (println "energy spent" energy-spent)
            (draw positions)
            (println))
          (cond
            (solved? preferred-rooms positions) {:path (get-path best-possible current)
                                                 :energy-spent energy-spent}
            (better best-possible current)
            (let [next-states (next-possible energy-spent preferred-rooms rooms floor-positions positions)
                  good-states (filter (partial better best-possible) next-states)]
              (recur (inc cntr)
                     (assoc best-possible (set positions) {:score energy-spent
                                                           :previous previous})
                     (reduce (fn [acc state]
                               (update acc (:guessed-cost state)
                                       add-to-set state))
                             (remove-current possibilities energy current) good-states)))
            :else (recur (inc cntr)
                         best-possible
                         (remove-current possibilities energy current))))
        (recur (inc cntr)
               best-possible
               (dissoc possibilities energy))))))

;; Part 1
(defn run-1 [input]
  (let [{:keys [preferred-rooms rooms positions floor-positions]} (parse input)]
    (find-best preferred-rooms rooms positions floor-positions)))

(defn run-test-1 []
  (run-1 test-input))

(defn run-real-1 []
  (run-1 real-input))


;; Part 2
(defn run-2 [input]
  (let [lines (string/split-lines input)
        new   (string/join "\n"
                      (concat (take 3 lines)
                             ["  #D#C#B#A#
  #D#B#A#C#  "]
                             (take 2 (drop 3 lines))))]
    (binding [draw draw-big]
      (run-1 new))))

(defn run-test-2 []
  (run-2 test-input))

(defn run-real-2 []
  (run-2 real-input))
