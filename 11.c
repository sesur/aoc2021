#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

char TestInput[] = "5483143223 \
2745854711 \
5264556173 \
6141336146 \
6357385478 \
4167524645 \
2176841721 \
6882881134 \
4846848554 \
5283751526";

char RealInput[] = "6318185732 \
1122687135 \
5173237676 \
8754362612 \
5718474666 \
8443654137 \
1247634346 \
1446514585 \
6717288267 \
1727871228";

typedef struct
{
	int Width, Height;
	int *Data;
} rectangle;

#define DATA(Rect, X, Y) ((Rect).Data[(X) + (Y) * (Rect).Width])
#define INDEX(Rect, X, Y) ((X) + (Y) * (Rect).Width)

rectangle Parse(char const *Str)
{
        rectangle Result;
        int Width = 0;
	int Height = 1;

	char const *Cur = Str;
	while (*Cur)
	{
                if (*Cur != ' ') {
			++Width;
		} else {
			Width = 0;
			++Height;
		}
		++Cur;
	}

	// to make matters a bit easier
	// we add a border around Data

	Width += 2;
	Height += 2;

	Result.Data   = malloc(sizeof(*Result.Data) * Width * Height);
	Result.Width  = Width;
	Result.Height = Height;

	for (int Y = 1;
	     Y < Height-1;
	     ++Y)
	{
		for (int X = 1;
		     X < Width-1;
		     ++X)
		{
			DATA(Result, X, Y) = Str[(X-1) + (Y-1) * (Width-1)] - '0';
		}
	}

	return Result;
}

void PrintRect(rectangle R)
{
	for (int Y = 1;
	     Y < R.Height-1;
	     ++Y)
	{
		for (int X = 1;
		     X < R.Width-1;
		     ++X)
		{
			printf("%d ", DATA(R, X, Y));
		}
		puts("");
	}
}

int Part1(char const *Input)
{
	int Result = 0;
	rectangle R = Parse(Input);

	int Top = 0;
	int *BlinkStack = malloc(R.Width * R.Height * sizeof(*BlinkStack));
	int *Blinked    = calloc(R.Width * R.Height, sizeof(*Blinked));

	rectangle BR;
	BR.Width  = R.Width;
	BR.Height = R.Height;
	BR.Data   = Blinked;

#define PUSH(X, Y)							\
	do {	assert(Top < R.Width * R.Height);			\
		if ((X) > 0 && (X) < (R.Width-1) && (Y) > 0		\
		    && (Y) < (R.Height-1) && !DATA(BR, (X), (Y)))	\
		{							\
			DATA(BR, (X), (Y)) = 1;				\
			BlinkStack[Top++] = INDEX(R, X, Y);		\
		}							\
	} while (0)
#define POP() (BlinkStack[--Top])
#define EMPTY() (Top == 0)

	for (int Step = 0;
	     Step < 100;
	     ++Step)
	{
		for (int Y = 1;
		     Y < R.Height-1;
		     ++Y)
		{
			for (int X = 1;
			     X < R.Width-1;
			     ++X)
			{
				DATA(BR, X, Y) = 0;
				DATA(R, X, Y) += 1;
				if (DATA(R, X, Y) > 9)
				{
					PUSH(X, Y);
				}
			}
		}

		while (!EMPTY())
		{
			int Index = POP();

			Result += 1;

			int X = Index % R.Width;
			int Y = Index / R.Width;

#define INCTEST(X, Y) do { DATA(R, (X), (Y)) += 1; if (DATA(R, (X), (Y)) > 9) PUSH((X), (Y));} while(0)

			INCTEST(X-1, Y-1);
			INCTEST(X-1,   Y);
			INCTEST(X-1, Y+1);

			INCTEST(X,   Y-1);
			INCTEST(X,   Y+1);

			INCTEST(X+1, Y-1);
			INCTEST(X+1,   Y);
			INCTEST(X+1, Y+1);
		}

		for (int Y = 1;
		     Y < R.Height-1;
		     ++Y)
		{
			for (int X = 1;
			     X < R.Width-1;
			     ++X)
			{
				if (DATA(R, X, Y) > 9)
				{
					DATA(R, X, Y) = 0;
				}
			}
		}
	}

#undef EMPTY
#undef POP
#undef PUSH


	free(BlinkStack);
	free(Blinked);
	free(R.Data);
	return Result;
}

int Part2(char const *Input)
{
	int Result = 0;
	rectangle R = Parse(Input);

	int Top = 0;
	int *BlinkStack = malloc(R.Width * R.Height * sizeof(*BlinkStack));
	int *Blinked    = calloc(R.Width * R.Height, sizeof(*Blinked));

	rectangle BR;
	BR.Width  = R.Width;
	BR.Height = R.Height;
	BR.Data   = Blinked;

#define PUSH(X, Y)							\
	do {	assert(Top < R.Width * R.Height);			\
		if ((X) > 0 && (X) < (R.Width-1) && (Y) > 0 && (Y) < (R.Height-1) && !DATA(BR, (X), (Y))) \
		{							\
			DATA(BR, (X), (Y)) = 1;			\
			BlinkStack[Top++] = INDEX(R, X, Y);		\
		}							\
	} while (0)
#define POP() (BlinkStack[--Top])
#define EMPTY() (Top == 0)

	int NumFish = (R.Width-2) * (R.Height-2);
	int NumBlinked;
        do
	{
		for (int Y = 1;
		     Y < R.Height-1;
		     ++Y)
		{
			for (int X = 1;
			     X < R.Width-1;
			     ++X)
			{
				DATA(BR, X, Y) = 0;
				DATA(R, X, Y) += 1;
				if (DATA(R, X, Y) > 9)
				{
					PUSH(X, Y);
				}
			}
		}

		NumBlinked = 0;

		while (!EMPTY())
		{
			int Index = POP();

			++NumBlinked;

			int X = Index % R.Width;
			int Y = Index / R.Width;

#define INCTEST(X, Y) do { DATA(R, (X), (Y)) += 1; if (DATA(R, (X), (Y)) > 9) PUSH((X), (Y));} while(0)

			INCTEST(X-1, Y-1);
			INCTEST(X-1,   Y);
			INCTEST(X-1, Y+1);

			INCTEST(X,   Y-1);
			INCTEST(X,   Y+1);

			INCTEST(X+1, Y-1);
			INCTEST(X+1,   Y);
			INCTEST(X+1, Y+1);
		}

		for (int Y = 1;
		     Y < R.Height-1;
		     ++Y)
		{
			for (int X = 1;
			     X < R.Width-1;
			     ++X)
			{
				if (DATA(R, X, Y) > 9)
				{
					DATA(R, X, Y) = 0;
				}
			}
		}
		Result += 1;
	} while (NumBlinked < NumFish);



	free(BlinkStack);
	free(Blinked);
	free(R.Data);
	return Result;
}

int main(int argc, char *argv[], char *envp[])
{
	printf("Part1 Test: (%d) Real: (%d)\n", Part1(TestInput), Part1(RealInput));
	printf("Part2 Test: (%d) Real: (%d)\n", Part2(TestInput), Part2(RealInput));
}
