:- use_module(library(clpfd)).

display(0, [0, 1, 2, 4, 5, 6]).
display(1, [2, 5]).
display(2, [0, 2, 3, 4, 6]).
display(3, [0, 2, 3, 5, 6]).
display(4, [1, 2, 3, 5]).
display(5, [0, 1, 3, 5, 6]).
display(6, [0, 1, 3, 4, 5, 6]).
display(7, [0, 2, 5]).
display(8, [0, 1, 2, 3, 4, 5, 6]).
display(9, [0, 1, 2, 3, 5, 6]).

in_0([A, B, C, D, E, F, G]) :-
    display(_, [B, E]).

in_1([A, B, C, D, E, F, G]) :-
    display(_, [C, F, B, E, G, A, D]).

in_2([A, B, C, D, E, F, G]) :-
    display(_, [C, B, D, G, E, F]).

in_3([A, B, C, D, E, F, G]) :-
    display(_, [F, G, A, E, C, D]).

in_4([A, B, C, D, E, F, G]) :-
    display(_, [C, G, E, B]).

in_5([A, B, C, D, E, F, G]) :-
    display(_, [F, D, C, G, E]).

in_6([A, B, C, D, E, F, G]) :-
    display(_, [A, G, E, B, F, D]).

in_7([A, B, C, D, E, F, G]) :-
    display(_, [F, E, C, D, B]).

in_8([A, B, C, D, E, F, G]) :-
    display(_, [F, A, B, C, D]).

in_9([A, B, C, D, E, F, G]) :-
    display(_, [E, D, B]).

line_1(X) :-
    in_0(X),
    in_1(X),
    in_2(X),
    in_3(X),
    in_4(X),
    in_5(X),
    in_6(X),
    in_7(X),
    in_8(X),
    in_9(X).

line_2(X).

run :-
    Vs = [A, B, C, D, E, F, G],
    Vs ins 0 .. 6,
    all_distinct(Vs),
    line_1([A, B, C, D, E, F, G]),
    line_2([A, B, C, D, E, F, G]).
