#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

typedef uint64_t u64;

#if defined(PART1)

typedef struct {
	u64 Score;
	u64 Place;
} player;

u64 Mod1(u64 Num, u64 Div)
{
	u64 Result = (Num - 1) % Div;
	return Result + 1;
}

int main(void)
{

	u64 Rolls = 0;

	player P[2];
	int Current = 1;
	P[0].Score = P[1].Score = 0;
#if defined(TEST)
        P[0].Place = 4;
	P[1].Place = 8;
#else
	P[0].Place = 8;
	P[1].Place = 1;
#endif

	do {
		Current = 1 - Current;
		u64 Result =  Mod1(Rolls + 1, 1000)
			    + Mod1(Rolls + 2, 1000)
			    + Mod1(Rolls + 3, 1000);
		Rolls += 3;

		P[Current].Place = Mod1(Result + P[Current].Place, 10);
		P[Current].Score += P[Current].Place;

	} while (P[Current].Score < 1000);

	printf("Rolls: %lu; Scores: %u, %u; Answer: %u * %u = %u\n",
	       Rolls, P[0].Score, P[1].Score, Rolls, P[1-Current].Score,
	       Rolls * P[1-Current].Score);

	return 0;
}

#else

u64 SafePlus(u64 a, u64 b, u64 Max)
{
	u64 Result = a + b;

	if (Result > Max)
	{
		Result = Max;
	}

	return Result;
}

int main(void)
{
	u64 Possibilities[7] = {1, 3, 6, 7, 6, 3, 1};

	u64 (*PositionScore)[2][10][10][22][22];
	PositionScore = malloc(sizeof(*PositionScore));

	memset(*PositionScore, 0, sizeof(*PositionScore));

	u64 Start1, Start2;

#define Player1 0
#define Player2 1

	u64 GamesFinishedAt[2][22];

	memset(GamesFinishedAt, 0, sizeof(GamesFinishedAt));

#if defined(TEST)
	Start1 = 4;
	Start2 = 8;
#else
	Start1 = 8;
	Start2 = 1;
#endif
	int Current  = 0;
	int Previous = 1;

	(*PositionScore)[Previous][Start1 - 1][Start2 - 1][0][0] = 1;

	for (u64 Rolls = 0;
	     Rolls < 21;
	     ++Rolls)
	{
		memset((*PositionScore)[Current], 0, sizeof((*PositionScore)[Current]));

		for (u64 P1Position = 0;
		     P1Position < 10;
		     ++P1Position)
		{
			u64 NextPositions[7];

			for (int i = 0; i < 7; ++i)
			{
				// possible dice throws are
				// 3, 4, 5, 6, 7, 8, 9
				NextPositions[i] = (P1Position + 3 + i) % 10;
			}

			for (u64 P2Position = 0;
			     P2Position < 10;
			     ++P2Position)
			{
				for (u64 P1Score = 0;
				     P1Score < 21;
				     ++P1Score)
				{
					for (u64 P2Score = 0;
					     P2Score < 21;
					     ++P2Score)
					{
						for (int j = 0;
						     j < 7;
						     ++j)
						{
							// the score of position x is x + 1
							// since positions go from 0 to 9
							// and scores go from 1 to 10
							u64 NextScore =
								SafePlus(P1Score, NextPositions[j]+1, 21);
							(*PositionScore)[Current][NextPositions[j]][P2Position][NextScore][P2Score]
								+= Possibilities[j]
								* (*PositionScore)[Previous][P1Position][P2Position][P1Score][P2Score];
						}
					}
				}
			}

		}

		for (u64 P1Position = 0;
		     P1Position < 10;
		     ++P1Position)
		{
			for (u64 P2Position = 0;
			     P2Position < 10;
			     ++P2Position)
			{
				assert((*PositionScore)[Current][P1Position][P2Position][21][21] == 0);
				for (u64 P2Score = 0;
				     P2Score < 21;
				     ++P2Score)
				{
					GamesFinishedAt[Player1][Rolls] += (*PositionScore)[Current][P1Position][P2Position][21][P2Score];
				}
			}
		}


		Current = 1 - Current;
		Previous = 1 - Previous;
		memset((*PositionScore)[Current], 0, sizeof((*PositionScore)[Current]));

		for (u64 P2Position = 0;
		     P2Position < 10;
		     ++P2Position)
		{
			u64 NextPositions[7];

			for (int i = 0; i < 7; ++i)
			{
				NextPositions[i] = (P2Position + 3 + i) % 10;
			}

			for (u64 P1Position = 0;
			     P1Position < 10;
			     ++P1Position)
			{
				for (u64 P2Score = 0;
				     P2Score < 21;
				     ++P2Score)
				{
					for (u64 P1Score = 0;
					     P1Score < 21;
					     ++P1Score)
					{
						for (int j = 0;
						     j < 7;
						     ++j)
						{
							u64 NextScore =
								SafePlus(P2Score, NextPositions[j]+1, 21);
							(*PositionScore)[Current][P1Position][NextPositions[j]][P1Score][NextScore]
								+= Possibilities[j]
								* (*PositionScore)[Previous][P1Position][P2Position][P1Score][P2Score];
						}
					}
				}
			}
		}

		for (u64 P2Position = 0;
		     P2Position < 10;
		     ++P2Position)
		{
			for (u64 P1Position = 0;
			     P1Position < 10;
			     ++P1Position)
			{
				assert((*PositionScore)[Current][P1Position][P2Position][21][21] == 0);
				for (u64 P1Score = 0;
				     P1Score < 21;
				     ++P1Score)
				{
					GamesFinishedAt[Player2][Rolls] += (*PositionScore)[Current][P1Position][P2Position][P1Score][21];
				}
			}
		}



#if defined(TEST)
		printf("-- ROLL %lu --\n", Rolls);
#endif
		Current = 1 - Current;
		Previous = 1 - Previous;
	}

	u64 P1Finished = 0;
	u64 P2Finished = 0;

	for (u64 Roll = 0;
	     Roll < 21;
	     ++Roll)
	{
#if defined(TEST)
		printf("%lu -- %lu\n", GamesFinishedAt[Player1][Roll], GamesFinishedAt[Player2][Roll]);
#endif
		P1Finished += GamesFinishedAt[Player1][Roll];
		P2Finished += GamesFinishedAt[Player2][Roll];
	}

	printf("Player 1: %lu\nPlayer 2: %lu\n", P1Finished, P2Finished);

	free(PositionScore);

	return 0;
}

#endif
